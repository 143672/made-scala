package homeworks.homework_2

import homeworks.homework_2.trait_linearization.PartO
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/***
 * Раскоментируйте тест и проверьте результат
 * */
class trait_linearization_test extends AnyFlatSpec with Matchers {
    val o = new PartO
    o.str shouldBe "YDOCHKA"
}
