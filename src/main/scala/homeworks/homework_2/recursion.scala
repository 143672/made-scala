package homeworks.homework_2

import scala.annotation.tailrec
import scala.math.BigInt

object recursion {

  /** *
   * Напишите функцию вычисления чисел фибоначи, используя хвостовую рекурсию @tailrec
   * */
  def fibonacci1(n: BigInt): BigInt = {
    if (n < 3)
      1
    else {
      fibonacci1(n-1) + fibonacci1(n-2)
    }
  }

  def fibonacci(n: BigInt): BigInt = {
    @tailrec
    def helper(counter: BigInt, num :BigInt, sum: BigInt): BigInt ={
      counter.compare(1) match  {
        case -1 => 0
        case 0 => num
        case 1 => helper(counter-1, sum, num + sum)
      }
    }

    helper(n, 1, 1)
  }
}
